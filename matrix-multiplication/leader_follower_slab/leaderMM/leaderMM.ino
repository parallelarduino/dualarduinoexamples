unsigned long startTic;                 // start time for stop watch
unsigned long compToc;                  // time for computation
unsigned long writeToc;                 // time for io
const int n=10;
const int nn=100;
double A[nn];
double B[nn];
double C[nn];

void setup() {
  // put your setup code here, to run once:
 
          // start MM
          for (int i=0; i<nn; i++){
             A[i]=0.1;
             B[i]=0.6;
             C[i]=0.5; 
          }
          
          // initialize serial communication at 9600 bits per second:
          Serial.begin(9600);
          startTic = micros();     // store the start time
          
          for (int i = 0; i <n/2; i++){
            for (int j = 0; j <n; j++){
              for (int k = 0; k <n; k++){
                C[i+j*n]+=A[i+k*n]*B[k+j*n];
              }
            }
          }
          compToc =   micros();   // computation end timestamp

           for (int i = 0; i < n/2; i++) {
            for (int j = 0; j < n; j++) {
              Serial.print(C[i+j*n]);
              Serial.print('|');
            }
            Serial.println(' ');
          }
        
          writeToc = micros();  // output end timestamp
          
          // Report elapsed times 
          Serial.write("Leader computation time:" );
          Serial.print( compToc - startTic); 
          Serial.write(" microseconds\n");
          Serial.write("Leader IO time:" );
          Serial.print( writeToc - compToc); 
          Serial.write(" microseconds\n");
}

void loop() {
  // put your main code here, to run repeatedly:

}
