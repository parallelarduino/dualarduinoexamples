/* A demonstration bottom up merge sort program */
// https://en.wikipedia.org/wiki/Merge_algorithm

// Used by https://github.com/bxparks/EpoxyDuino
// for emulation
#include <Arduino.h>
#include <Wire.h>

const unsigned int RANDNUMSPOW = 8;
const unsigned int RANDNUMS = 1 << RANDNUMSPOW;
unsigned int vals[RANDNUMS];
unsigned int temp[RANDNUMS];
unsigned int step[RANDNUMSPOW];
const unsigned int left = 0;
const unsigned int right = RANDNUMS - 1;
const unsigned int myleft = 0;
const unsigned int myright = (RANDNUMS - 1)/2;
char store[20];

void setup()
{

 unsigned int i;
 unsigned int j;
 
 unsigned int head;
 unsigned int headleft;
 unsigned int headright;

 unsigned long tic;
 unsigned long tocsort1;
 unsigned long tocsort2;
 unsigned long toccommunicate;

 Wire.begin();
 Serial.begin(9600);
 
 for(i = 0; i < RANDNUMSPOW; i++) {
         step[i] = 1 << (i+1);
 }
 Serial.write("Sorting ");
 Serial.print(RANDNUMS);
 Serial.write(" numbers.\n");
 randomSeed(analogRead(1));
 for(i = myleft; i <= myright; i++) {
   vals[i] = random(0, 65535);
 }

 tic = micros();
 // outmost loop is over the levels
 for(j = 0; j<RANDNUMSPOW-1; j++)
 {
   for(i = myleft; i <= myright; i++) temp[i] = vals[i];
   for(i = myleft; i <= myright; i += step[j])
   {
     head = i;
     headleft = i;
     headright = i + step[j]/2;
     while((headleft<i+step[j]/2)&&(headright<i+step[j]))
     {
       if(temp[headleft]<temp[headright])
       {
         vals[head++]=temp[headleft++];
       }else{
         vals[head++]=temp[headright++];
       }
     }
     while(headleft<i+step[j]/2) vals[head++]=temp[headleft++];
     while(headright<i+step[j]) vals[head++]=temp[headright++];
   }
 }
 tocsort1 = micros();
 // Get data from follower
 for(i=myright+1;i<=right;i++){
  Wire.requestFrom(1,sizeof(int));
  int count=0;
  while(Wire.available()) {
   store[count++]=Wire.read();
  }
  temp[i]=strtoul(store, NULL, 0);
 }
 toccommunicate = micros();
 // Final merge
 for(i=myleft;i<=myright;i++) temp[i]=vals[i];

 head=left;
 headleft=left;
 headright=left+step[RANDNUMSPOW-1]/2;
 while((headleft<left+step[RANDNUMSPOW-1]/2)&&(headright<left+step[RANDNUMSPOW-1]))
 {
   if(temp[headleft]<temp[headright])
   {
     vals[head++]=temp[headleft++];
   }else{
     vals[head++]=temp[headright++];
   }
 }
 while(headleft<left+step[RANDNUMSPOW-1]/2) vals[head++]=temp[headleft++];
 while(headright<left+step[RANDNUMSPOW-1]) vals[head++]=temp[headright++];
 tocsort2 = micros();

 bool validate = true;
 for(i=left; i<right; i++) {
   if (vals[i] > vals[i+1]) {
     validate = false;
   }
 }
 if (!validate) {
   Serial.write("Sort unsuccessful\n");
 }else{
   Serial.write("Sorting time :");
   Serial.print(tocsort2-toccommunicate+tocsort1-tic);
   Serial.write(" microseconds\n");
   Serial.write("Communication time :");
   Serial.print(toccommunicate-tocsort1);
   Serial.write(" microseconds\n");
   Serial.write("Total time :");
   Serial.print(tocsort2-tic);
   Serial.write(" microseconds\n");
 }
}

void loop() {
  // put your main code here, to run repeatedly:

}

